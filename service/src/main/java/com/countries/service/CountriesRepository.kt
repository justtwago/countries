package com.countries.service

import android.content.SharedPreferences
import androidx.core.content.edit
import com.countries.domain.model.Country
import com.countries.domain.model.Millis
import com.countries.service.api.CountriesService
import com.countries.service.api.call.Response
import com.countries.service.database.CountriesDao
import com.countries.service.model.toDomain
import com.countries.service.model.toEntity
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import java.util.*
import com.countries.domain.repository.CountriesRepository as Repository

private const val LAST_UPDATE_KEY = "LAST_UPDATE_KEY"
private const val AUTO_UPDATE_TRACKER_DELAY: Millis = 1000
private const val UNKNOWN_LAST_UPDATE = -1L

internal class CountriesRepository(
    private val service: CountriesService,
    private val dao: CountriesDao,
    private val preferences: SharedPreferences
) : Repository {
    private val autoUpdateTracker = ticker(delayMillis = AUTO_UPDATE_TRACKER_DELAY.toLong(), initialDelayMillis = 0)

    override suspend fun observeCountries(autoUpdatePeriod: Millis): Flow<List<Country>> {
        return autoUpdateTracker
            .consumeAsFlow()
            .onEach { handleAutoUpdatingCountries(autoUpdatePeriod) }
            .distinctUntilChanged()
            .combine(dao.observeAll()) { _, countries -> countries }
            .map { countries -> countries.map { it.toDomain() } }
    }

    private suspend fun handleAutoUpdatingCountries(autoUpdatePeriod: Millis) {
        val lastUpdateDateMillis = preferences.getLong(LAST_UPDATE_KEY, UNKNOWN_LAST_UPDATE)
        if (lastUpdateDateMillis == UNKNOWN_LAST_UPDATE) {
            synchronizeCountries()
            return
        }

        val lastUpdateDateCalendar = Calendar.getInstance().apply { timeInMillis = lastUpdateDateMillis }
        val nowCalendar = Calendar.getInstance()
        lastUpdateDateCalendar.set(Calendar.MILLISECOND, autoUpdatePeriod)
        if (lastUpdateDateCalendar.timeInMillis <= nowCalendar.timeInMillis) {
            synchronizeCountries()
        }
    }

    override suspend fun synchronizeCountries() {
        val response = service.getAllCountries()
        val countriesResponse = (response as? Response.Success)?.body.orEmpty()
        val countries = countriesResponse.mapNotNull { it.toEntity() }
        dao.insertAll(countries)
        if (countries.isNotEmpty()) {
            preferences.edit { putLong(LAST_UPDATE_KEY, Calendar.getInstance().timeInMillis) }
        }
    }
}