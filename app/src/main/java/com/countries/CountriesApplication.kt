package com.countries

import android.app.Application
import com.countries.di.AppComponentProvider
import com.countries.di.DaggerAppComponent
import com.countries.service.di.DaggerServiceComponent

class CountriesApplication : Application(), AppComponentProvider {

    private val serviceComponent by lazy {
        DaggerServiceComponent
            .factory()
            .create(context = this)
    }

    override val appComponent by lazy {
        DaggerAppComponent
            .factory()
            .create(serviceComponent, applicationContext = this)
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        serviceComponent.inject(this)
    }
}