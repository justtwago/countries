package com.countries.service.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.countries.service.CountriesRepository  as CountriesRepositoryImpl
import com.countries.domain.repository.CountriesRepository
import com.countries.service.api.CountriesService
import com.countries.service.api.call.ResponseAdapter
import com.countries.service.database.CountriesDao
import com.countries.service.database.CountriesDatabase
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit

private const val SHARED_PREFERENCES_KEY = "SHARED_PREFERENCES_KEY"

@Module
internal object ServiceModule {

    @Provides
    internal fun sharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE)

    @Provides
    internal fun countriesRepository(
        service: CountriesService,
        dao: CountriesDao,
        preferences: SharedPreferences
    ): CountriesRepository = CountriesRepositoryImpl(service, dao, preferences)

    @Provides
    internal fun countriesDatabase(context: Context): CountriesDatabase =
        Room.databaseBuilder(context, CountriesDatabase::class.java, CountriesDatabase.NAME).build()

    @Provides
    internal fun countriesDao(database: CountriesDatabase): CountriesDao = database.countriesDao()

    @Provides
    internal fun countriesService(
        retrofitBuilder: Retrofit.Builder,
        okHttpClient: OkHttpClient
    ): CountriesService = retrofitBuilder
        .client(okHttpClient)
        .baseUrl("https://restcountries.eu/rest/v2/")
        .build()
        .create()

    @Provides
    internal fun retrofitBuilder(moshi: Moshi): Retrofit.Builder = Retrofit.Builder()
        .addCallAdapterFactory(ResponseAdapter.Factory)
        .addConverterFactory(MoshiConverterFactory.create(moshi))

    @Provides
    internal fun moshi(): Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @Provides
    internal fun okHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .callTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .build()
}