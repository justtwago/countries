package com.countries.domain.model

data class Country(
    val name: String,
    val flagUrl: String?,
    val currencies: List<String>,
    val phoneNumberCodes: List<String>,
    val internetDomainNames: List<String>
)
