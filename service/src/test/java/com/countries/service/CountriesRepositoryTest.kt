package com.countries.service

import android.content.SharedPreferences
import androidx.core.content.edit
import com.countries.domain.model.Country
import com.countries.service.api.CountriesService
import com.countries.service.api.call.Response
import com.countries.service.database.CountriesDao
import com.countries.service.model.CountryEntity
import com.countries.service.model.CountryResponse
import com.countries.service.model.Currency
import com.countries.service.utils.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.kotlintest.shouldBe
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class CountriesRepositoryTest {
    @Mock private lateinit var service: CountriesService
    @Mock private lateinit var dao: CountriesDao
    @Mock private lateinit var preferences: SharedPreferences
    @InjectMocks private lateinit var repository: CountriesRepository

    private val mockedSharedPreferencesEditor = mock<SharedPreferences.Editor>()

    @Test
    fun `observe countries in the database`() = runBlockingTest {
        val countryEntities = listOf(
            CountryEntity(
                name = "name",
                flagUrl = "flag",
                currencies = listOf("currency"),
                phoneNumberCodes = listOf("code"),
                internetDomainNames = listOf("domain")
            )
        )
        whenever(dao.observeAll()).thenReturn(flowOf(countryEntities))

        repository.observeCountries(autoUpdatePeriod = 0).test {
            expectItem() shouldBe listOf(
                Country(
                    name = "name",
                    flagUrl = "flag",
                    currencies = listOf("currency"),
                    phoneNumberCodes = listOf("code"),
                    internetDomainNames = listOf("domain")
                )
            )
            cancel()
        }
    }

    @Test
    fun `synchronize countries database with backend each 5 seconds if 5 minutes left`() = runBlockingTest {
        whenever(dao.observeAll()).thenReturn(emptyFlow())
        whenever(preferences.getLong("LAST_UPDATE_KEY", -1))
            .thenReturn(Calendar.getInstance().apply { timeInMillis -= 5000 }.timeInMillis)

        repository.observeCountries(autoUpdatePeriod = 5000).test {
            verify(service).getAllCountries()
            cancel()
        }
    }


    @Test
    fun `synchronize countries database with backend if there is no last update date`() = runBlockingTest {
        whenever(dao.observeAll()).thenReturn(emptyFlow())
        whenever(preferences.getLong("LAST_UPDATE_KEY", -1)).thenReturn(-1)

        repository.observeCountries(autoUpdatePeriod = 5000).test {
            verify(service).getAllCountries()
            cancel()
        }
    }

    @Test
    fun `do not synchronize countries database with backend each 5 seconds if 5 minutes did not leave`() =
        runBlockingTest {
            whenever(dao.observeAll()).thenReturn(emptyFlow())
            whenever(preferences.getLong("LAST_UPDATE_KEY", -1))
                .thenReturn(Calendar.getInstance().apply { timeInMillis -= 3000 }.timeInMillis)

            repository.observeCountries(autoUpdatePeriod = 5000).test {
                verify(service, never()).getAllCountries()
                cancel()
            }
        }

    @Test
    fun `fetch countries, put it into the database and save last update date`() = runBlockingTest {
        val response = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("currency")),
            callingCodes = listOf("code"),
            topLevelDomains = listOf("domain")
        )
        whenever(service.getAllCountries()).thenReturn(Response.Success(listOf(response)))
        whenever(preferences.edit()).thenReturn(mockedSharedPreferencesEditor)

        repository.synchronizeCountries()

        val countryEntities = listOf(
            CountryEntity(
                name = "name",
                flagUrl = "flag",
                currencies = listOf("currency"),
                phoneNumberCodes = listOf("code"),
                internetDomainNames = listOf("domain")
            )
        )
        verify(dao).insertAll(countryEntities)
        verify(mockedSharedPreferencesEditor).putLong(any(), any())
    }

    @Test
    fun `fetch empty countries after failure response and update database`() = runBlockingTest {
        whenever(service.getAllCountries()).thenReturn(Response.Failure(Exception()))

        repository.synchronizeCountries()

        verify(dao).insertAll(emptyList())
    }
}