package com.countries.ui

import CoroutinesMainDispatcherRule
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.countries.domain.model.Country
import com.countries.domain.repository.CountriesRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {
    @get:Rule val coroutinesRule = CoroutinesMainDispatcherRule()
    @get:Rule val instantExecutorRule = InstantTaskExecutorRule()
    @Mock private lateinit var repository: CountriesRepository
    private lateinit var viewModel: MainViewModel
    private val countriesObserver = mock<Observer<List<Country>>>()

    @Test
    fun `fetch countries to live data`() = runBlockingTest {
        val countries = listOf(
            Country(
                name = "name",
                flagUrl = "flagUrl",
                currencies = listOf("currency"),
                phoneNumberCodes = listOf("code"),
                internetDomainNames = listOf("domain")
            )
        )
        whenever(repository.observeCountries(86400000)).thenReturn(flowOf(countries))

        viewModel = MainViewModel(repository)
        viewModel.countriesLiveData.observeForever(countriesObserver)

        verify(countriesObserver).onChanged(countries)
    }

    @Test
    fun `fetch empty countries to live data`() = runBlockingTest {
        whenever(repository.observeCountries(86400000)).thenReturn(flowOf(emptyList()))

        viewModel = MainViewModel(repository)
        viewModel.countriesLiveData.observeForever(countriesObserver)

        verify(countriesObserver).onChanged(emptyList())
    }
}