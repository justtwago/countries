package com.countries.ui.list

import com.countries.domain.model.Country

data class CountryItem(
    val name: String,
    val flagUrl: String?,
    var isExpanded: Boolean,
    val currencies: List<String>,
    val phoneNumberCodes: List<String>,
    val internetDomainNames: List<String>
) {
    val formattedCurrencies get() = currencies.joinToString(separator = "\n")
    val formattedPhoneNumberCodes get() = phoneNumberCodes.joinToString(separator = "\n") { "+$it" }
    val formattedInternetDomainNames get() = internetDomainNames.joinToString(separator = "\n")
}

fun Country.toItem() = CountryItem(
    name = name,
    flagUrl = flagUrl,
    currencies = currencies,
    phoneNumberCodes = phoneNumberCodes,
    internetDomainNames = internetDomainNames,
    isExpanded = false
)
