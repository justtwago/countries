package com.countries.service.api

import com.countries.service.api.call.Response
import com.countries.service.model.CountryResponse
import retrofit2.http.GET

internal interface CountriesService {
    @GET("all")
    suspend fun getAllCountries(): Response<List<CountryResponse>>
}
