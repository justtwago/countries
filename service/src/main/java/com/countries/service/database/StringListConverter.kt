package com.countries.service.database

import androidx.room.TypeConverter

class StringListConverter {
    @TypeConverter
    fun toString(list: List<String>): String = list.joinToString(separator = ";")

    @TypeConverter
    fun toList(string: String): List<String> = string.split(";")
}
