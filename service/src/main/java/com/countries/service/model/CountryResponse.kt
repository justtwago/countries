package com.countries.service.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class CountryResponse(
    @Json(name = "name") val name: String?,
    @Json(name = "flag") val flag: String?,
    @Json(name = "currencies") val currencies: List<Currency>?,
    @Json(name = "callingCodes") val callingCodes: List<String>?,
    @Json(name = "topLevelDomain") val topLevelDomains: List<String>?
)

@JsonClass(generateAdapter = true)
internal data class Currency(
    @Json(name = "name") val name: String?
)
