package com.countries.service.api.call

import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response as RetrofitResponse

internal class ResponseCall<T : Any>(
    private val delegate: Call<T>,
    private val responseMapper: (RetrofitResponse<T>) -> Response<T>
) : Call<Response<T>> {
    override fun execute(): RetrofitResponse<Response<T>> {
        val response = try {
            val retrofitResponse = delegate.execute()
            responseMapper(retrofitResponse)
        } catch (throwable: Throwable) {
            Response.Failure(throwable)
        }
        return RetrofitResponse.success(response)
    }

    override fun enqueue(callback: Callback<Response<T>>) {
        delegate.enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, throwable: Throwable) {
                val response = Response.Failure(throwable)
                callback.onResponse(this@ResponseCall, RetrofitResponse.success(response))
            }

            override fun onResponse(call: Call<T>, retrofitResponse: RetrofitResponse<T>) {
                val response = responseMapper(retrofitResponse)
                callback.onResponse(this@ResponseCall, RetrofitResponse.success(response))
            }
        })
    }

    override fun clone() = ResponseCall(delegate.clone(), responseMapper)
    override fun request(): Request = delegate.request()
    override fun cancel() = delegate.cancel()
    override fun isCanceled() = delegate.isCanceled
    override fun isExecuted() = delegate.isExecuted
}
