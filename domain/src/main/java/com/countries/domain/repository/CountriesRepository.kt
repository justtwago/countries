package com.countries.domain.repository

import com.countries.domain.model.Country
import com.countries.domain.model.Millis
import kotlinx.coroutines.flow.Flow

interface CountriesRepository {
    suspend fun observeCountries(autoUpdatePeriod: Millis): Flow<List<Country>>
    suspend fun synchronizeCountries()
}