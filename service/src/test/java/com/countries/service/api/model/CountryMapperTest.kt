package com.countries.service.api.model

import com.countries.service.model.CountryEntity
import com.countries.service.model.CountryResponse
import com.countries.service.model.Currency
import com.countries.service.model.toEntity
import io.kotlintest.shouldBe
import org.junit.Test


class CountryMapperTest {

    @Test
    fun `map response with full data and single currency, phone code and internet domain`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("currency")),
            callingCodes = listOf("code"),
            topLevelDomains = listOf("domain")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("currency"),
            phoneNumberCodes = listOf("code"),
            internetDomainNames = listOf("domain")
        )
    }

    @Test
    fun `map response with full data and multiple currency, phone code and internet domain`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("firstCurrency"), Currency("secondCurrency")),
            callingCodes = listOf("firstCode", "secondCode"),
            topLevelDomains = listOf("firstDomain", "secondDomain")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("firstCurrency", "secondCurrency"),
            phoneNumberCodes = listOf("firstCode", "secondCode"),
            internetDomainNames = listOf("firstDomain", "secondDomain")
        )
    }

    @Test
    fun `map response to null if there is no name`() {
        val countryResponse = CountryResponse(
            name = null,
            flag = "flag",
            currencies = listOf(Currency("firstCurrency"), Currency("secondCurrency")),
            callingCodes = listOf("firstCode", "secondCode"),
            topLevelDomains = listOf("firstDomain", "secondDomain")
        )

        val country = countryResponse.toEntity()

        country shouldBe null
    }

    @Test
    fun `map response with nullable flag`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = null,
            currencies = listOf(Currency("firstCurrency"), Currency("secondCurrency")),
            callingCodes = listOf("firstCode", "secondCode"),
            topLevelDomains = listOf("firstDomain", "secondDomain")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = null,
            currencies = listOf("firstCurrency", "secondCurrency"),
            phoneNumberCodes = listOf("firstCode", "secondCode"),
            internetDomainNames = listOf("firstDomain", "secondDomain")
        )
    }

    @Test
    fun `map response with nullable currencies`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = null,
            callingCodes = listOf("firstCode", "secondCode"),
            topLevelDomains = listOf("firstDomain", "secondDomain")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = emptyList(),
            phoneNumberCodes = listOf("firstCode", "secondCode"),
            internetDomainNames = listOf("firstDomain", "secondDomain")
        )
    }

    @Test
    fun `map response with nullable phone number codes`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("firstCurrency"), Currency("secondCurrency")),
            callingCodes = null,
            topLevelDomains = listOf("firstDomain", "secondDomain")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("firstCurrency", "secondCurrency"),
            phoneNumberCodes = emptyList(),
            internetDomainNames = listOf("firstDomain", "secondDomain")
        )
    }

    @Test
    fun `map response with nullable internet domain names`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("firstCurrency"), Currency("secondCurrency")),
            callingCodes = listOf("firstCode", "secondCode"),
            topLevelDomains = null
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("firstCurrency", "secondCurrency"),
            phoneNumberCodes = listOf("firstCode", "secondCode"),
            internetDomainNames = emptyList()
        )
    }

    @Test
    fun `map response with empty currency`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("")),
            callingCodes = listOf("firstCode", "secondCode"),
            topLevelDomains = listOf("firstDomain", "secondDomain")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = emptyList(),
            phoneNumberCodes = listOf("firstCode", "secondCode"),
            internetDomainNames = listOf("firstDomain", "secondDomain")
        )
    }

    @Test
    fun `map response with empty phone number code`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("firstCurrency"), Currency("secondCurrency")),
            callingCodes = listOf(""),
            topLevelDomains = listOf("firstDomain", "secondDomain")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("firstCurrency", "secondCurrency"),
            phoneNumberCodes = emptyList(),
            internetDomainNames = listOf("firstDomain", "secondDomain")
        )
    }

    @Test
    fun `map response with empty internet domain name`() {
        val countryResponse = CountryResponse(
            name = "name",
            flag = "flag",
            currencies = listOf(Currency("firstCurrency"), Currency("secondCurrency")),
            callingCodes = listOf("firstCode", "secondCode"),
            topLevelDomains = listOf("")
        )

        val country = countryResponse.toEntity()

        country shouldBe CountryEntity(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("firstCurrency", "secondCurrency"),
            phoneNumberCodes = listOf("firstCode", "secondCode"),
            internetDomainNames = emptyList()
        )
    }

}