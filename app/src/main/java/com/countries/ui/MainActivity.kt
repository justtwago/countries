package com.countries.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.observe
import com.countries.databinding.ActivityMainBinding
import com.countries.ui.extensions.viewModel
import com.countries.ui.list.CountriesAdapter
import com.countries.ui.list.toItem

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val countriesAdapter by lazy { CountriesAdapter() }
    private val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBinding()
        setupCountriesList()
        setupListeners()
        setupObservers()
    }

    private fun setupBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater).apply { setContentView(root) }
    }

    private fun setupCountriesList() {
        binding.countriesRecyclerView.adapter = countriesAdapter
    }

    private fun setupListeners() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.synchronizeCountries()
        }
    }

    private fun setupObservers() {
        viewModel.countriesLiveData.observe(this) { countries ->
            binding.swipeRefreshLayout.isRefreshing = false
            countriesAdapter.submitList(countries.map { it.toItem() })
        }
    }
}
