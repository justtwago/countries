package com.countries.service.api.call

internal sealed class Response<out T : Any> {
    data class Success<T : Any>(val body: T) : Response<T>()
    data class Failure(val throwable: Throwable) : Response<Nothing>()
}