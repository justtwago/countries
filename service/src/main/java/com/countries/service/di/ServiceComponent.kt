package com.countries.service.di

import android.content.Context
import com.countries.domain.repository.CountriesRepository
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.BindsInstance
import dagger.Component

@Component(modules = [ServiceModule::class])
interface ServiceComponent {

    val countriesRepository: CountriesRepository

    fun inject(context: Context) {
        AndroidThreeTen.init(context)
    }

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): ServiceComponent
    }
}