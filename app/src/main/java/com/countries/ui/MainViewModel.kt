package com.countries.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.countries.domain.model.Country
import com.countries.domain.model.Millis
import com.countries.domain.repository.CountriesRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val DAY: Millis = 86400000

class MainViewModel @Inject constructor(private val repository: CountriesRepository) : ViewModel() {
    private val _countriesLiveData = MutableLiveData<List<Country>>()
    val countriesLiveData: LiveData<List<Country>> = _countriesLiveData

    init {
        viewModelScope.launch {
            repository.observeCountries(autoUpdatePeriod = DAY).collect {
                _countriesLiveData.value = it
            }
        }
    }

    fun synchronizeCountries() {
        viewModelScope.launch { repository.synchronizeCountries() }
    }
}