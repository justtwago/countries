package com.countries.ui.extensions

import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.lifecycle.ViewModel
import com.countries.di.appComponent

inline fun <reified VM : ViewModel> ComponentActivity.viewModel() = viewModels<VM> { appComponent.viewModelFactory }