package com.countries.ui.list

import com.countries.domain.model.Country
import io.kotlintest.shouldBe
import org.junit.Test

class CountryItemTest {

    @Test
    fun `map country to item`() {
        val country = Country(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("currency"),
            phoneNumberCodes = listOf("code"),
            internetDomainNames = listOf("domain")
        )

        val item = country.toItem()

        item shouldBe CountryItem(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("currency"),
            phoneNumberCodes = listOf("code"),
            internetDomainNames = listOf("domain"),
            isExpanded = false
        )
    }

    @Test
    fun `format multiple currencies`() {
        val country = CountryItem(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("firstCurrency", "secondCurrency"),
            phoneNumberCodes = listOf("code"),
            internetDomainNames = listOf("domain"),
            isExpanded = false
        )


        country.formattedCurrencies shouldBe "firstCurrency\nsecondCurrency"
    }

    @Test
    fun `format phone number code`() {
        val country = CountryItem(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("currency"),
            phoneNumberCodes = listOf("code"),
            internetDomainNames = listOf("domain"),
            isExpanded = false
        )


        country.formattedPhoneNumberCodes shouldBe "+code"
    }

    @Test
    fun `format multiple phone number codes`() {
        val country = CountryItem(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("currency"),
            phoneNumberCodes = listOf("firstCode", "secondCode"),
            internetDomainNames = listOf("domain"),
            isExpanded = false
        )


        country.formattedPhoneNumberCodes shouldBe "+firstCode\n+secondCode"
    }

    @Test
    fun `format multiple internet domain names`() {
        val country = CountryItem(
            name = "name",
            flagUrl = "flag",
            currencies = listOf("currency"),
            phoneNumberCodes = listOf("code"),
            internetDomainNames = listOf("firstDomain", "secondDomain"),
            isExpanded = false
        )


        country.formattedInternetDomainNames shouldBe "firstDomain\nsecondDomain"
    }
}