package com.countries.ui.list

import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.countries.R
import com.countries.databinding.ItemCountryBinding
import com.countries.ui.extensions.renderImage
import com.countries.utils.AnimatedTransition

class CountriesViewHolder(
    private val binding: ItemCountryBinding,
    private val parent: ViewGroup
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: CountryItem) {
        with(binding) {
            renderData(item)
            renderDetailsVisibility(item)
            root.setOnClickListener {
                TransitionManager.beginDelayedTransition(parent, AnimatedTransition())
                item.isExpanded = !item.isExpanded
                renderDetailsVisibility(item)
            }
        }
    }

    private fun ItemCountryBinding.renderData(item: CountryItem) {
        nameTextView.text = item.name
        currencyTextView.text = item.formattedCurrencies
        internetDomainTextView.text = item.formattedInternetDomainNames
        phoneNumberCodeTextView.text = item.formattedPhoneNumberCodes
        item.flagUrl?.let { flagImageView.renderImage(imageUrl = it, placeholderId = R.drawable.ic_black_flag) }
    }

    private fun ItemCountryBinding.renderDetailsVisibility(item: CountryItem) {
        detailsDividerView.isVisible = item.isExpanded
        currencyTextView.isVisible = item.currencies.isNotEmpty() and item.isExpanded
        internetDomainTextView.isVisible = item.internetDomainNames.isNotEmpty() and item.isExpanded
        phoneNumberCodeTextView.isVisible = item.phoneNumberCodes.isNotEmpty() and item.isExpanded
        moreImageView.setImageResource(if (item.isExpanded) R.drawable.ic_arrow_up else R.drawable.ic_arrow_down)
    }
}
