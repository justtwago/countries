package com.countries.ui.extensions

import android.net.Uri
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.bumptech.glide.request.RequestOptions
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou

fun ImageView.renderImage(imageUrl: String, @DrawableRes placeholderId: Int) {
    GlideToVectorYou
        .init()
        .with(context)
        .requestBuilder
        .load(Uri.parse(imageUrl))
        .placeholder(ContextCompat.getDrawable(context, placeholderId))
        .into(this)
}
