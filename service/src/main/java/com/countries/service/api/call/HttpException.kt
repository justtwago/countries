package com.countries.service.api.call

data class HttpException(val httpCode: Int) : Exception() {
    override val message: String get() = "Http code is $httpCode"
}