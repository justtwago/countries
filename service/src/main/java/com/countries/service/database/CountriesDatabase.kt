package com.countries.service.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.countries.service.model.CountryEntity

@Database(entities = [CountryEntity::class], version = 1)
@TypeConverters(value = [StringListConverter::class])
internal abstract class CountriesDatabase : RoomDatabase() {

    abstract fun countriesDao(): CountriesDao

    companion object {
        const val NAME = "countries-database"
    }
}