package com.countries.service.api.call

import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.Response as RetrofitResponse
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

internal class ResponseAdapter<T : Any>(
    private val type: Type
) : CallAdapter<T, Call<Response<T>>> {
    override fun responseType() = type

    override fun adapt(call: Call<T>) = ResponseCall(call, ::mapToResponse)

    private fun mapToResponse(response: RetrofitResponse<T>): Response<T> {
        if (!response.isSuccessful) return Response.Failure(HttpException(response.code()))
        val responseBody = response.body() ?: return Response.Failure(NullPointerException("Response body is null"))
        return Response.Success(responseBody)
    }

    object Factory : CallAdapter.Factory() {
        override fun get(
            returnType: Type,
            annotations: Array<Annotation>,
            retrofit: Retrofit
        ): CallAdapter<*, *>? {
            val rawType = getRawType(returnType)
            if (rawType != Call::class.java) return null

            val callType = getParameterUpperBound(0, returnType as ParameterizedType)
            if (getRawType(callType) != Response::class.java) return null

            val responseType = getParameterUpperBound(0, callType as ParameterizedType)
            return ResponseAdapter<Any>(responseType)
        }
    }
}
