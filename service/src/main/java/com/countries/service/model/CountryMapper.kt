package com.countries.service.model

import com.countries.domain.model.Country

internal fun CountryResponse.toEntity(): CountryEntity? {
    return CountryEntity(
        name = name ?: return null,
        flagUrl = flag,
        currencies = currencies
            ?.mapNotNull { it.name }
            ?.filterNot { it.isBlank() }
            .orEmpty(),
        phoneNumberCodes = callingCodes
            ?.filterNot { it.isBlank() }
            .orEmpty(),
        internetDomainNames = topLevelDomains
            ?.filterNot { it.isBlank() }
            .orEmpty()
    )
}


fun CountryEntity.toDomain() = Country(
    name = name,
    flagUrl = flagUrl,
    currencies = currencies,
    phoneNumberCodes = phoneNumberCodes,
    internetDomainNames = internetDomainNames
)
