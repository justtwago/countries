package com.countries.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.countries.service.di.ServiceComponent
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [ServiceComponent::class],
    modules = [ViewModelModule::class]
)
interface AppComponent {

    val viewModelFactory: ViewModelProvider.Factory

    fun inject(applicationContext: Context)

    @Component.Factory
    interface Factory {
        fun create(
            serviceComponent: ServiceComponent,
            @BindsInstance applicationContext: Context
        ): AppComponent
    }
}

interface AppComponentProvider {
    val appComponent: AppComponent
}

val Context.appComponent: AppComponent
    get() = (applicationContext as AppComponentProvider).appComponent