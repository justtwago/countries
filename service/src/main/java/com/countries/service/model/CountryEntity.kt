package com.countries.service.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CountryEntity(
    @PrimaryKey val name: String,
    @ColumnInfo(name = "flag_url") val flagUrl: String?,
    @ColumnInfo(name = "phone_number_codes") val phoneNumberCodes: List<String>,
    @ColumnInfo(name = "internet_domain_names") val internetDomainNames: List<String>,
    val currencies: List<String>
)