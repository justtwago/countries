# Counties App
Mobile Android application for showing a list of countries of the world.

### Master build status
[![Build Status](https://app.bitrise.io/app/5fb1f4b096c90537/status.svg?token=KIQlBBD0lCEw2Mo6BzJs4g&branch=master)](https://app.bitrise.io/app/5fb1f4b096c90537)

### Trello Board
[https://trello.com/b/dI9xWvtJ/countries](https://trello.com/b/dI9xWvtJ/countries)

### Copyrights
Icons made by [Smashicons](https://www.flaticon.com/authors/smashicons) from [www.flaticon.com](https://www.flaticon.com/)
